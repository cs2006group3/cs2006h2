
module Board where
  
import Data.Bool 

data Col = Black | White
  deriving (Show, Eq)

other :: Col -> Col
other Black = White
other White = Black

type Position = (Int, Int)

-- A Board is a record containing the board size (a board is a square grid, n *
-- n), the number of consecutive passes, and a list of pairs of position and
-- the colour at that position.

data Board = Board { size :: Int,
                     passes :: Int,
                     pieces :: [(Position, Col)]
                   }
  deriving Show

-- Default board is 8x8, neither played has passed, with 4 initial pieces 
initBoard = Board 8 0 [((3,3), White), ((3, 4), Black),
                       ((4,3), Black), ((4,4), White)]

-- Overall state is the board and whose turn it is, plus any further
-- information about the world (this may later include, for example, player
-- names, timers, information about rule variants, etc)
--
-- Feel free to extend this, and 'Board' above with anything you think
-- will be useful (information for the AI, for example, such as where the
-- most recent moves were).
data World = World { board :: Board,
                     turn :: Col }

initWorld = World initBoard Black

-- Play a move on the board; return 'Nothing' if the move is invalid
-- (e.g. outside the range of the board, there is a piece already there,
-- or the move does not flip any opposing pieces)
makeMove :: Board -> Col -> Position -> Maybe Board 
makeMove board colour (x,y) = if isValid board x y colour then Just (Board (size board) (passes board) ((pieces board) ++ [((x,y), colour)]))
                              else Nothing

isValid :: Board -> Int -> Int -> Col ->Bool
isValid b x y colour = inBoard x && emptyCircle x y (pieces b) && nextToAPiece x y b && not (gameOver b) && checksFlipPiece x y colour b

inBoard :: Int-> Bool
inBoard x = if x <= 7 then True else False

emptyCircle :: Int -> Int -> [(Position, Col)] -> Bool
emptyCircle x y (b:bs) = do let posx = fst (fst (b))
                            let posy = snd (fst (b))
                            if x == posx && y == posy then False
                            else emptyCircle x y bs
emptyCircle x y [] = True

nextToAPiece :: Int -> Int -> Board -> Bool
nextToAPiece x y board = check1 x y (pieces board ) || check2 x y (pieces board ) || check3 x y (pieces board ) || check4 x y (pieces board ) ||
                         check5 x y (pieces board ) || check6 x y (pieces board ) || check7 x y (pieces board ) || check8 x y (pieces board )

check1 :: Int -> Int -> [(Position, Col)] -> Bool
check1 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x-1== posx && y-1== posy then True
                       else check1 x y bs
check1 x y [] = False

check2 :: Int -> Int -> [(Position, Col)] -> Bool
check2 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x== posx && y-1== posy then True
                       else check2 x y bs
check2 x y [] = False

check3 :: Int -> Int -> [(Position, Col)] -> Bool
check3 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x+1== posx && y-1== posy then True
                       else check3 x y bs
check3 x y [] = False

check4 :: Int -> Int -> [(Position, Col)] -> Bool
check4 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x-1== posx && y== posy then True
                       else check4 x y bs
check4 x y [] = False

check5 :: Int -> Int -> [(Position, Col)] -> Bool
check5 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x+1== posx && y== posy then True
                       else check5 x y bs
check5 x y [] = False

check6 :: Int -> Int -> [(Position, Col)] -> Bool
check6 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x-1== posx && y+1== posy then True
                       else check6 x y bs
check6 x y [] = False

check7 :: Int -> Int -> [(Position, Col)] -> Bool
check7 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x== posx && y+1== posy then True
                       else check7 x y bs
check7 x y [] = False

check8 :: Int -> Int -> [(Position, Col)] -> Bool
check8 x y (b:bs) = do let posx = fst (fst (b))
                       let posy = snd (fst (b))
                       if x+1== posx && y+1== posy then True
                       else check8 x y bs
check8 x y [] = False

checkScore :: [(Position, Col)] -> (Int, Int)
checkScore pieces = (length $ filter (isColour White) pieces, length $ filter (isColour Black) pieces)
            where
              isColour col (pos,c) = col == c
checksFlipPiece :: Int -> Int -> Col -> Board -> Bool
checksFlipPiece x y colour board = check1p x y colour (pieces board ) (pieces board )|| check2p x y colour (pieces board ) (pieces board )|| check3p x y colour (pieces board ) (pieces board )|| check4p x y colour (pieces board ) (pieces board )||
                       check5p x y colour (pieces board ) (pieces board )|| check6p x y colour (pieces board ) (pieces board )|| check7p x y colour (pieces board ) (pieces board )|| check8p x y colour (pieces board ) (pieces board )
                       
check1p :: Int -> Int -> Col -> [(Position, Col)] -> [(Position, Col)] -> Bool
check1p x y colour (b:bs) pieces= do let posx = fst (fst (b))
                                     let posy = snd (fst (b))
                                     let col = snd (b)
                                     if x-1== posx && y-1== posy && col/=colour then do let xEnd = fst (checkLine x y colour (-1) (-1) pieces)
                                                                                        let yEnd = snd (checkLine x y colour (-1) (-1) pieces)
                                                                                        if xEnd < 8 && yEnd < 8 then do let xchange = -1
                                                                                                                        let ychange = -1
                                                                                                                        if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                        else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                        else False
                                     else check1p x y colour bs pieces
check1p x y colour [] pieces = False

check2p :: Int -> Int -> Col-> [(Position, Col)] -> [(Position, Col)] -> Bool
check2p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x== posx && y-1== posy && col/= colour then do let xEnd = fst (checkLine x y colour (0) (-1) pieces)
                                                                                        let yEnd = snd (checkLine x y colour (0) (-1) pieces)
                                                                                        if xEnd < 8 && yEnd < 8 then do let xchange = 0
                                                                                                                        let ychange = -1
                                                                                                                        if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                        else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                        else False
                                      else check2p x y colour bs pieces
check2p x y colour [] pieces = False

check3p :: Int -> Int -> Col-> [(Position, Col)] ->[(Position, Col)] -> Bool
check3p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x+1== posx && y-1== posy && col/=colour then do let xEnd = fst (checkLine x y colour (1) (-1) pieces)
                                                                                         let yEnd = snd (checkLine x y colour (1) (-1) pieces)
                                                                                         if xEnd < 8 && yEnd < 8 then do let xchange = 1
                                                                                                                         let ychange = -1
                                                                                                                         if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                         else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                         else False
                                      else check3p x y colour bs pieces
check3p x y colour [] pieces = False

check4p :: Int -> Int -> Col-> [(Position, Col)] -> [(Position, Col)] -> Bool
check4p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x-1== posx && y== posy && col/=colour then do let xEnd = fst (checkLine x y colour (-1) (0) pieces)
                                                                                       let yEnd = snd (checkLine x y colour (-1) (0) pieces)
                                                                                       if xEnd < 8 && yEnd < 8 then do let xchange = -1
                                                                                                                       let ychange = 0
                                                                                                                       if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                       else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                       else False
                                      else check4p x y colour bs pieces
check4p x y colour [] pieces = False

check5p :: Int -> Int -> Col-> [(Position, Col)] ->[(Position, Col)] -> Bool
check5p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x+1== posx && y== posy && col/=colour then do let xEnd = fst (checkLine x y colour (1) (0) pieces)
                                                                                       let yEnd = snd (checkLine x y colour (1) (0) pieces)
                                                                                       if xEnd < 8 && yEnd < 8 then do let xchange = 1
                                                                                                                       let ychange = 0
                                                                                                                       if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                       else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                       else False
                                      else check5p x y colour bs pieces
check5p x y colour [] pieces = False

check6p :: Int -> Int -> Col-> [(Position, Col)] -> [(Position, Col)] -> Bool
check6p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x-1== posx && y+1== posy && col/=colour then do let xEnd = fst (checkLine x y colour (-1) (1) pieces)
                                                                                         let yEnd = snd (checkLine x y colour (-1) (1) pieces)
                                                                                         if xEnd < 8 && yEnd < 8 then do let xchange = -1
                                                                                                                         let ychange = 1
                                                                                                                         if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces
                                                                                                                         else flipPieces xEnd yEnd colour x y xchange ychange pieces
                                                                                         else False
                                      else check6p x y colour bs pieces
check6p x y colour [] pieces = False

check7p :: Int -> Int -> Col-> [(Position, Col)] -> [(Position, Col)] -> Bool
check7p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x== posx && y+1== posy && col/=colour then do let xEnd = fst (checkLine x y colour (0) (1) pieces)
                                                                                       let yEnd = snd (checkLine x y colour (0) (1) pieces)
                                                                                       if xEnd < 8 && yEnd < 8 then do let xchange = 0
                                                                                                                       let ychange = 1
                                                                                                                       if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces 
                                                                                                                       else flipPieces xEnd yEnd colour x y xchange ychange pieces 
                                                                                       else False
                                      else check7p x y colour bs pieces
check7p x y colour [] pieces = False

check8p :: Int -> Int -> Col-> [(Position, Col)] ->[(Position, Col)] ->  Bool
check8p x y colour (b:bs) pieces = do let posx = fst (fst (b))
                                      let posy = snd (fst (b))
                                      let col = snd (b)
                                      if x+1== posx && y+1== posy && col/=colour then do let xEnd = fst (checkLine x y colour (1) (1) pieces)
                                                                                         let yEnd = snd (checkLine x y colour (1) (1) pieces)
                                                                                         if xEnd < 8 && yEnd < 8 then do let xchange = 1
                                                                                                                         let ychange = 1
                                                                                                                         if x+y < xEnd+yEnd && x<xEnd then flipPieces x y colour xEnd yEnd xchange ychange pieces 
                                                                                                                         else flipPieces xEnd yEnd colour x y xchange ychange pieces                                                     
                                                                                         else False
                                      else check8p x y colour bs pieces
check8p x y colour [] pieces = False

checkLine :: Int -> Int -> Col-> Int -> Int -> [(Position, Col)] -> (Int,Int)
checkLine x y col xchange ychange pieces = do let posx = fst (fst (checkArray x y xchange ychange pieces))
                                              let posy = snd (fst (checkArray x y xchange ychange pieces))
                                              let closeCol = snd (checkArray x y xchange ychange pieces)
                                              if posx <8 || posy <8 then do let xEnd=posx
                                                                            let yEnd=posy
                                                                            if col==closeCol then (xEnd,yEnd)
                                                                            else checkLine posx posy col xchange ychange pieces
                                              else (9,9)
                                              
checkArray :: Int -> Int -> Int -> Int -> [(Position, Col)] -> (Position, Col)
checkArray x y xchange ychange (b:bs) = do let posx = fst (fst (b))
                                           let posy = snd (fst (b))
                                           let col = snd (b)
                                           if (x+xchange) == posx && (y+ychange) == posy then ((posx,posy),col)
                                           else checkArray x y xchange ychange bs
checkArray x y xchange ychange [] = ((9,9),White)

-- changePiecex saves the x position to change the colour of the piece
-- changePiecey saves the y position to change the colour of fiece
-- colour is the colour to be changed to
flipPieces :: Int -> Int -> Col -> Int -> Int -> Int -> Int -> [(Position, Col)] -> Bool
flipPieces x y colour xEnd yEnd xchange ychange (b:bs) = if x/=xEnd && y/=yEnd then do let changePiecex = fst ( fst (b))
                                                                                       let changePiecey = snd ( fst (b))
                                                                                       flipPieces (x-xchange) (y-ychange) colour xEnd yEnd xchange ychange bs
                                                         else flipPieces (x-xchange) (y-ychange) colour xEnd yEnd xchange ychange bs
flipPieces x y colour xEnd yEnd xchange ychange [] = True
--  or there have been two consecutive passes)
gameOver :: Board -> Bool
gameOver board = do let numberOfPieces= (size board) *(size board)
                    if length (pieces board) == numberOfPieces then True
                    else False
-- An evaluation function for a minimax search. Given a board and a colour
-- return an integer indicating how good the board is for that colour.
--evaluate :: Board -> Col -> Int
--evaluate = undefined