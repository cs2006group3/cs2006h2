module Input(handleInput) where

import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss
import Board
import AI
import Debug.Trace

startX=(-(1000 `div` 2))
startY=(800 `div` 2)
-- Update the world state given an input event. Some sample input events
-- are given; when they happen, there is a trace printed on the console
--
-- trace :: String -> a -> a
-- 'trace' returns its second argument while printing its first argument
-- to stderr, which can be a very useful way of debugging!
handleInput :: Event -> World -> World
handleInput (EventMotion (x, y)) b 
    = trace ("Mouse moved to: " ++ show ((coordinates x y))) b
handleInput (EventKey (MouseButton LeftButton) Up m (x, y)) b 
      = case makeMove (board b) (turn b) (coordinates x y) of
          Nothing -> b
          Just x -> World x (other (turn b))

--    = trace ("Left button pressed at: " ++ show (clickMade x y)) b
handleInput (EventKey (Char k) Down _ _) b
    = trace ("Key " ++ show k ++ " down") b
handleInput (EventKey (Char k) Up _ _) b
    = trace ("Key " ++ show k ++ " up") b
handleInput e b = b

{- Hint: when the 'World' is in a state where it is the human player's
 turn to move, a mouse press event should calculate which board position
 a click refers to, and update the board accordingly.

 At first, it is reasonable to assume that both players are human players.
-}
coordinates :: Float -> Float -> (Int, Int)
coordinates x y = ((-startX+ (round x)) `div` 100,(startY- (round y)) `div` 100)

