module Draw(drawWorld) where

import Graphics.Gloss
import Board


startx=(-(1000/2))+50
starty=(800/2)-50
--sizeOfPiece=800/

drawWorld :: World -> Picture
drawWorld w = do let boardSize = size (board w) 
                 Pictures [drawGrid (boardSize), drawInitBoard boardSize w, drawScores (pieces (board w))]
 
drawGrid :: Int -> Picture
drawGrid numSquares = Pictures (map drawBoard [(i,j) | i <- [0..numSquares-1], j <- [0..numSquares-1]])      


drawBoard :: (Int, Int) -> Picture
drawBoard (x,y) = Pictures [(color squareColor $ (Translate  (startx+(100*(fromIntegral x))) (starty-(100*(fromIntegral y))) (rectangleSolid 100 100)))
                   ,(color circleColor $ (Translate  (startx+(100*(fromIntegral x))) (starty-(100*(fromIntegral y))) (circle 50)))]
				   where
					   squareColor = light black
					   circleColor = light (light black)
drawInitBoard :: Int -> World -> Picture
drawInitBoard boardSize w = drawPieces (pieces (board w))

drawPieces :: [(Position, Col)] -> Picture
drawPieces [] = Pictures[]
drawPieces (x:xs) = Pictures [drawPiece x, drawPieces xs] 

drawPiece :: (Position, Col) -> Picture
drawPiece ((x,y), Black) = (color black $ (Translate  (startx+(100*(fromIntegral x))) (starty-(100*(fromIntegral y))) (circleSolid 50)))
drawPiece ((x,y), White) = (color white $ (Translate  (startx+(100*(fromIntegral x))) (starty-(100*(fromIntegral y))) (circleSolid 50)))

drawScores :: [(Position, Col)] -> Picture
drawScores pieces = let  whiteCount = fst (checkScore (pieces ))
                         blackCount = snd (checkScore (pieces ))
                         in Pictures [scale 0.3 0.4 (color white (Translate (1000) (800)(text "White"))), scale 0.36 0.376 (color white (Translate (1100) (850) (text "Black")))
                        , scale 0.32 0.3 (color white (Translate (1100) (850) (text (show whiteCount)))), scale 0.37 0.3 (color white (Translate (1100) (850) (text (show blackCount))))]
